<?php

namespace App;


class Student
{
    private $studentName;
    private $studentId;
    private $address;

    public $info;

    public function setStudentName($studentName)
    {
        $this->studentName = $studentName;
    }

    public function setStudentId($studentId)
    {
        $this->studentId = $studentId;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function setInfo()
    {
        $this->info = $this->studentName." ".$this->studentId." ".$this->address;

    }

    public function getInfo()
    {
        $this->setInfo();
        echo $this->info;
    }
}