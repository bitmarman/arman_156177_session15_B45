<?php

namespace App;


class Course{
    private $bengaliMarks;
    private $englishMarks;
    private $mathMarks;

    private $grade;

    public $gradeEnglish;
    public $gradeBengali;
    public $gradeMath;
    public $result;

    public function setBengaliMarks($bengaliMarks)
    {
        $this->bengaliMarks = $bengaliMarks;
    }

    public function setEnglishMarks($englishMarks)
    {
        $this->englishMarks = $englishMarks;
    }

    public function setMathMarks($mathMarks)
    {
        $this->mathMarks = $mathMarks;
    }

    public function gradeCount($value){
        switch($value){
            case $value>=80: $this->grade="A+";
                break;
            case $value>=70: $this->grade="A";
                break;
            case $value>=60: $this->grade="B";
                break;
            case $value>=50: $this->grade="C";
                break;
        }
        return $this->grade;
    }

    public function getGradeBengali()
    {
        return $this->gradeBengali=$this->gradeCount($this->bengaliMarks);
    }

    public function getGradeEnglish()
    {
        return $this->gradeEnglish=$this->gradeCount($this->englishMarks);
    }

    public function getGradeMath()
    {
        return $this->gradeMath=$this->gradeCount($this->mathMarks);
    }

    public function getResult(){
        return $this->Result();
    }
}